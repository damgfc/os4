/*
$Author: o-moring $
$Log: oss.c,v $
Revision 1.1  2015/03/31 12:30:50  o-moring
Initial revision

$Date: 2015/03/31 12:30:50 $
*/

/******************/
/** Derek Moring **/
/**   Project 4  **/
/**     oss.c    **/
/******************/

sharedStruct *shmP;
PCBArray *pcbP;
int pidArray[21];
static int k = 0; // counter for the pidArray

int main(int argc, char **argv){
	char filename[15] = "main.log";
	int shmid, i, j, k, waitTime, maxChild, bitVector[18];
	char consumerArg[3];
	pid_t child;
	
	maxChild = 18;
	for (i = 0; i < maxChild; i++){
		bitVector[i] = 0;
	}
	
	/** Register the alarms to catch **/
	signal(SIGALRM, signalHandling);
	signal(SIGINT, signalHandling);
	
	/** Create Shared Memory and Attach **/
	shmid = shmget(SHM_KEY, sizeof(sharedStruct), IPC_CREAT | 0777);
	
	if (shmid == -1){
		perror("Unsuccessful shmget");
		exit(1);
	}
	else {
		shmP = (sharedStruct *)(shmat(shmid, 0, 0));
		if (shmP == (void *)-1) {
			return -1;	
		}
	}	

	alarm(100); // Start the alarm
	
	k = 0;
	/** Go through loop to create 1 producer and n consumers **/
	while(1){
	
		/** Update Clock **/
		increaseClock(shmP->clockSeconds, shmP->clockNanoseconds);
		
		if (k < 18) {
			shmP->pcbP[k]->timeAccumulated = 0;
			shmP->pcbP[k]->totalTime = 0;
			shmP->pcbP[k]->priority = 0;
			shmP->pcbP[k]->lastBurst = 0;

			child = fork();
			waitTime = randomNumberGenerator(0, 20); //How long to start process
			sleep(waitTime);

			switch(child) {
				case -1:
						perror("Unable to fork\n");
						exit(1);
				case 0:
						execl("./user", "user", NULL); //Exec to user
						break;
				default:
						shmP->pcbP[k]->processID = getpid();
						shmP->pcbP[k]->parentProcessID = getppid();
						pidArray[k] = child; //Add pid to array and increment
						k++;										
						break;
			}
		}
	}
	
	detatchAndRemove(shmid, sharedStruct);
	return 1;
}

/*** Increase the clock ***/
void increaseClock(int *clockSeconds, int *clockNanoseconds){
    int nanoseconds;

    nanoseconds = randomNumGen(0, 1000);

    // Add the milliseconds to clock
    clockNanoseconds += milliseconds;
    if (clockNanoseconds >= 1000){
        // Math makes it >=1000 so increment the int
        //and take difference for decimal
        clockSeconds += 1;
        clockNanoseconds -= 1000;
    }

    // Add 1 to the clock int. Not a duplicate of above increment
    clockSeconds += 1;
}


/***
pidArray[0] = Producer 
pidArray[1] = Consumer01
pidArray[2] = Consumer02
pidArray[N] = Master
***/

