#ifndef SHARED_H
#define SHARED_H

#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <semaphore.h>

#define SHM_KEY 4255


typedef struct {
	int processID;
	int parentProcessID;
	int userID;
	double totalTime;
	double timeAccumulated;
	int priority;
	int lastBurst;	
} PCB; 


typedef struct {
	int pidArray[21];
	sem_t semaphore;
	unsigned int clockSeconds;  //in seconds
	unsigned int clockNanoseconds; //in nanoseconds
	PCB PCBArray[18];
	
} sharedStruct;

void writeToLog(char *filename, char *string);
char *getCurrentTime();
void signalHandling(int sigNum, int array[], int size);
void detachAndRemove(int shmid, void *shmaddr);
void removeLogFiles();
#endif
