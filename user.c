/*
$Author: o-moring $
$Log: user.c,v $
Revision 1.1  2015/03/31 12:30:50  o-moring
Initial revision

$Date: 2015/03/31 12:30:50 $
*/

/******************/
/** Derek Moring **/
/**   Project 4  **/
/**    user.c    **/
/******************/
#include <stdio.h>
#include <semaphore.h>
#include <sys/ipc.h>
#include "shared.h"
#include "user.h"

#define QUANTUM 100

sharedStruct *shmP;
int main(int argc, char **argv){
	int bUseAllQuantum, quantumToUse, pid;
	
	if (argc > 1){
		pid = atoi(argv[1]);
	}

	/** Register the alarms to catch **/
	signal(SIGALRM, signalHandling);
	signal(SIGINT, signalHandling);
	
	/** Get shared memory access **/
	int shmid = shmget (SHM_KEY, sizeof(sharedStruct), 0777);
	
	if (shmid == -1){
		perror("Error in child shmget");
		exit(1);
	}
	shmP = (sharedStruct *)(shmat(shmid, 0, 0));
		
	/** Constantly check shm if it has been scheduled
		if it is, start running **/
	while (1){
		while(shmP->schedule[id] == 1){
			sem_wait(shmP->semaphore);
			// Determine if using all quantum
			bUseAllQuantum = randomNumGen(0, 1);
			
			if (bUseAllQuantum == 0){
				quantumToUse = randomNumGen(0, QUANTUM);
			}
			else {
				quantumToUse = QUANTUM;
			}
			
			//Update Process Control BLock by adding to accumulated Time		
			shmP->PCBArray[pid]->lastBurst = quantumToUse;
			shmP->PCBArray[pid]->totalTime += quantumToUse;
			
			shmP->schedule[pid] = 0;
			
			sem_post(shmP->semaphore);
		}
	}

	


	// join readyqueue

	// if 50 milliseconds accumulated
	// 	generate random number to see if process completed
	//	if done, message to oss who should remove it from PCB

}



